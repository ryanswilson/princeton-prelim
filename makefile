SHELL := /bin/bash

.PHONY: global
global: penv django-sentinel ~/bin/gunicorn_start ~/logs/gunicorn-error.log ~/run nginx superconf
	sudo supervisorctl restart princeton-prelim

# .PHONY:utd
# utd:
# 	hg checkout tip

full: utd global
	sudo apt-get update
	sudo apt-get -y install nginx supervisor python3-venv python-dev gcc libpq-dev
	sudo apt-get -y install python3-pip python3-venv python3-wheel 
	sudo systemctl enable supervisor
	sudo systemctl start supervisor

penv: requirements.txt
	python3 -m venv penv
	source penv/bin/activate; pip install wheel;pip install -r requirements.txt;pip install gunicorn

DJANGOFILESA= $(wildcard prelim/*)
DJANGOFILESB= $(wildcard main/*)
django-sentinel: $(DJANGOFILESA) $(DJANGOFILESB) penv
	source penv/bin/activate; python manage.py migrate
	-source penv/bin/activate; python manage.py collectstatic
	python manage.py loaddata main/fixtures/prelim_dat.yaml
	python manage.py loaddata main/fixtures/prelim_q.yaml
	python manage.py loaddata main/fixtures/prelim_soln.yaml
	touch django-sentinel

~/bin:
	mkdir ~/bin

~/bin/gunicorn_start: ~/bin gunicorn_start.temp penv
	cp ./gunicorn_start.temp ~/bin/gunicorn_start
	chmod u+x ~/bin/gunicorn_start

~/run:
	mkdir ~/run

~/logs:
	mkdir ~/logs

~/logs/gunicorn-error.log: ~/logs
	touch ~/logs/gunicorn-error.log

.PHONY: superconf
superconf: /etc/supervisor/conf.d/princeton-prelim.conf

/etc/supervisor/conf.d/princeton-prelim.conf: conf.temp
	sudo cp conf.temp /etc/supervisor/conf.d/princeton-prelim.conf
	sudo supervisorctl reread
	sudo supervisorctl update
	sudo supervisorctl status princeton-prelim

.PHONY: nginx 
nginx: /etc/nginx/sites-available/princetonprelim.com ~/logs /etc/nginx/sites-enabled/princetonprelim.com
	sudo service nginx restart


/etc/nginx/sites-available/princetonprelim.com: nginx.conf
	sudo cp nginx.conf /etc/nginx/sites-available/princetonprelim.com

/etc/nginx/sites-enabled/princetonprelim.com: 
	sudo ln -s /etc/nginx/sites-available/princetonprelim.com /etc/nginx/sites-enabled/princetonprelim.com
	-sudo rm /etc/nginx/sites-enabled/default
