from django.shortcuts import render, redirect, get_object_or_404

from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse
from django.contrib.admin.views.decorators import staff_member_required

from .forms import tagForm,editForm,massTag

from .models import Prelim,Question,Solution,CommunitySolution
from taggit.models import Tag

import bleach
from main._form_aid import convert_to_mathjax
# Create your views here.

prelim_num=35

def index(request):
    test_list=[Prelim.objects.get(pk=i) for i in range(35) ]
    test_list.reverse()
    q_and_tags=lambda x: [x,x.tags.all()]
    test_q_list=[[test, map(q_and_tags,test.question_set.all())] for test in test_list]
    
    context={
        'tests':test_q_list
    }
    return render(request,'main/index.html',context)

def about(request):
    return render(request,'main/about.html')


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            #user.save()
            login(request, user)
            return redirect('main:index')
    else:
        form = UserCreationForm()
    return render(request, 'main/register.html', {'form': form})

def prelim_view(request,prelim_id):
    head=get_object_or_404(Prelim,pk=prelim_id)
    questions=head.question_set.all()
    q_an_l=[[q,q.solution_set.all(),q.communitysolution_set.all()] for q in questions]

    nextn=head.ID+1
    if nextn>34:
        nextn=None
    prevn=head.ID-1
    if prevn<0:
        prevn=None
    context={
        'head':head,
        'solns':q_an_l,
        'nextn':nextn,
        'prevn':prevn,
    }
    return render(request,'main/prelim.html',context)

def vote_view(request,soln_id,val):
     soln=get_object_or_404(Solution,pk=soln_id)
     host_q=Question.objects.get(pk=soln.question.ID)
     host_p=Prelim.objects.get(pk=host_q.prelim.ID)

     if int(val)==0:
         soln.votes.up(request.user.id)
     else:
         soln.votes.down(request.user.id)

     return redirect('main:prelim_view',prelim_id=host_p.ID)
     
def prob_view(request,prob_id):
    prob=get_object_or_404(Question,pk=prob_id)
    
    if request.method == 'POST':
        if request.user.is_authenticated:
            form = tagForm(request.POST)
            if form.is_valid():
                #prob.tags.add(*form.value.split(' '))
                #print(form.value.split(' '))
                ftxt=form.cleaned_data.get('tag_text')
                #uhhhh maybe fix this??
                prob.tags.add(*ftxt.split(','))
        else:
            form=tagForm()
    else:
        form=tagForm()

    pdflink=None
    lnfm=prob.long_form
    #test=Prelim.objects.get(pk=prob.prelim)
    test=prob.prelim
    if lnfm[:4]!='2013' or test.season!=1:
        testn=lnfm[:4]+['J','M'][test.season]
        linkname=testn+lnfm[-1]+lnfm[-2]+'.pdf'
        pdflink='/static/main/q_pdfs/'+testn+'/'+linkname
        
    solns=prob.solution_set.all()
    tags=prob.tags.all()
    comm_soln=prob.communitysolution_set.all()

    
    pid=int(prob_id)-1
    if pid<0:
        pid=None
    nid=int(prob_id)+1
    if nid>419:
        nid=None
    
    context={
        'prob':prob,
        'solns':solns,
        'tags':tags,
        'form':form,
        'pdflink':pdflink,
        'comm_soln':comm_soln,
        'pid':pid,
        'nid':nid,
    }
    return render(request,'main/question.html',context)

def search(request):
    qset=Question.objects.none()
    searches=request.GET.get("question-search","").split(' ')
    for sf in searches:
        tset=Tag.objects.filter(slug__icontains=sf)
        for t in tset:
            qset=qset|Question.objects.filter(tags__name__in=[t])
    context={
        'questions':qset.distinct()
    }
    return render(request,'main/browse.html',context)


def community_soln(request,prob_id):
    prob=get_object_or_404(Question,pk=prob_id)
    soln=prob.communitysolution_set.first()
    context={
        'soln':soln,
    }
    return render(request,'main/community.html',context)

def edit_soln(request,prob_id):
    if request.user.is_authenticated:
        prob=get_object_or_404(Question,pk=prob_id)
        soln=prob.communitysolution_set.first()
        if request.method == 'POST':
            form=editForm(request.POST)
            if form.is_valid():
                ftxt=bleach.clean(form.cleaned_data.get('comm_text'))
                ftxt=convert_to_mathjax(ftxt)
                if not soln:
                    soln=CommunitySolution.objects.create(soln_text=ftxt,question=prob,changed_by=request.user)
                else:
                    soln.soln_text=ftxt
                    soln.changed_by=request.user
                
                soln.save()
            return redirect('main:community_soln',prob_id=prob_id)
        else:
            if soln:
                form=editForm(initial={'comm_text':soln.soln_text})
            else:
                form=editForm()
            context={
                'soln':soln,
                'form':form,
            }
            return render(request,'main/edit_comm.html',context)
    else:
        return redirect('main:community_soln',prob_id=prob_id)



@staff_member_required
def massTagView(request):

    if request.method=='POST':
        form=massTag(request.POST)
        if form.is_valid():
            form.save()

    form=massTag()
    context={
        'form':form
    }
    return render(request,'admin/massTag.html',context)

    
