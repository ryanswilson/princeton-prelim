# Generated by Django 2.1.3 on 2018-11-09 19:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20181108_0253'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='solution',
            name='score',
        ),
        migrations.AddField(
            model_name='solution',
            name='num_vote_down',
            field=models.PositiveIntegerField(db_index=True, default=0),
        ),
        migrations.AddField(
            model_name='solution',
            name='num_vote_up',
            field=models.PositiveIntegerField(db_index=True, default=0),
        ),
        migrations.AddField(
            model_name='solution',
            name='vote_score',
            field=models.IntegerField(db_index=True, default=0),
        ),
    ]
