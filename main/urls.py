from django.urls import path, re_path
from django.conf.urls import include

from . import views

app_name='main'
urlpatterns = [
    path('', views.index, name='index'),
    path('register/',views.register,name='register'),
    #path('login/',views.login,name='login'),
    #path('logout/',views.logout,name='logout'),
    path('prelim/<prelim_id>/',views.prelim_view,name='prelim_view'),
    path('soln/<soln_id>/vote/<val>',views.vote_view,name="vote_view"),
    path('problem/<prob_id>/',views.prob_view,name="prob_view"),
    #re_path(r'^browse/(?P<browseparam>.*)',views.search,name='search'),
    re_path(r'^browse/',views.search,name='search'),
    path('about/',views.about,name="about"),
    path('soln/community/<prob_id>/',views.community_soln,name="community_soln"),
    path('soln/community/<prob_id>/edit',views.edit_soln,name="edit_soln")
]
