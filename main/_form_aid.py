import re


q_re=re.compile(r'([jJmM][0-9]{2})([mMeEqQtT])([1-3])')

#'J98M1'

subdict={
    'm':0,
    'e':1,
    'q':2,
    't':3,
}

def build_prelims_dict():
    years=range(1998,2017)
    mono_years=[2011,2014,2016]
    prelims={}
    lm=['j','m']
    i=0
    for year in years:
        prelims[lm[0]+str(year)[2:]]=i
        i+=1
        if year not in mono_years:
            prelims[lm[1]+str(year)[2:]]=i
            i+=1
    return prelims

revdict=build_prelims_dict()

def reverse_qid(qstr):
    #try:
    grp=q_re.match(qstr)
    pid=revdict[grp.group(1).lower()]
    sqid=3*subdict[grp.group(2).lower()]+int(grp.group(3))-1
    #except:
     #   return None
    return 12*pid+sqid

nlre=re.compile(r'\\newline')
sdre=re.compile(r'(?<=[^\$])\$(?=[^\$])')
nullre=re.compile(r'\\documentclass.*{.*}|\\begin{document}|\\end{document}')

def convert_to_mathjax(text):
    proc_text='\n'.join(nlre.split(text))
    proc_text=alternate_join(sdre.split(proc_text),'\(','\)')
    proc_text=''.join(nullre.split(proc_text))
    return proc_text


def alternate_join(slist,first,second):
    acc=slist.pop(0)
    t=True
    while slist:
        if t:
            acc+=first+slist.pop(0)
            t=False
        else:
            acc+=second+slist.pop(0)
            t=True
    return acc
    
