from django.db import models

from vote.models import VoteModel
from taggit.managers import TaggableManager
from simple_history.models import HistoricalRecords


# Create your models here.
class Prelim(models.Model):
    ID=models.AutoField(primary_key=True)
    year=models.IntegerField()
    season=models.IntegerField()
    abrev=models.CharField(max_length=6,null=True)


class Question(models.Model):
    ID=models.AutoField(primary_key=True)
    prelim=models.ForeignKey(Prelim,on_delete=models.CASCADE)
    Subject=models.IntegerField()
    number=models.IntegerField()
    default=models.CharField(max_length=20)
    tags=TaggableManager()
    long_form=models.CharField(max_length=10)

    
class Solution(VoteModel,models.Model):
    ID=models.AutoField(primary_key=True)
    question=models.ForeignKey(Question,on_delete=models.CASCADE)
    soln_ref=models.URLField()
    name=models.CharField(max_length=100)
#    score=models.FloatField(default=0.5)

class CommunitySolution(models.Model):
    ID=models.AutoField(primary_key=True)
    question=models.ForeignKey(Question,on_delete=models.CASCADE)
    soln_text=models.TextField()
    history=HistoricalRecords()
    changed_by=models.ForeignKey('auth.User',on_delete=models.SET_NULL,null=True)

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def history_user(self,value):
        self.changed_by=value
