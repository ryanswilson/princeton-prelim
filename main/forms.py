from django import forms
from .models import Question

import bleach

from ._form_aid import reverse_qid

class tagForm(forms.Form):
    tag_text=forms.CharField(label='add tags',max_length=100)

class editForm(forms.Form):
    comm_text=forms.CharField(widget=forms.Textarea)

class massTag(forms.Form):
    tag_field=forms.CharField()
    prob_field=forms.CharField()
    
    def save(self,commit=True):
        tag_txt=bleach.clean(self.cleaned_data.get('tag_field',None))
        p_txt=bleach.clean(self.cleaned_data.get('prob_field',None))
        tags=tag_txt.split(',')
        #format : J98M1
        for pf in p_txt.split(','):
            qid=reverse_qid(pf.replace(' ',''))
            prob=Question.objects.get(pk=qid)
            prob.tags.add(*tags)
      
