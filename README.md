## Princeton Prelim
This is the source for [princetonprelim.com](https://princetonprelim.com/). Cloning the repository and running make (with root) should be sufficient to get a clone of the website up and running on a ubuntu 18.04 machine. This website was made using Django to better organize the study questions for the Princeton physics preliminary exam. 
